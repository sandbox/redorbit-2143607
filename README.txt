scheduled_help_popup
--------------------

DESCRIPTION:

This module creates a popup block that may be inserted into an arbitrary region.
After an adjustable time, this block opens a popup window with help information
for the user:

* Some Text
* Contact partner
* Phone number

These values can be set by an administration interface.

Multilanguage is supported. By this different contact partners and phone
numbers may be supplied depending on the language.

The module comes with an own css file, so it can be themed as wished. 