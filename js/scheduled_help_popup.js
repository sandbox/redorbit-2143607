jQuery(document).ready(function() {

  // Anzeigen des Blocks nach einer bestimmten Zeit
	var wait_time = Drupal.settings.ro_help.wait_time * 1000;
	jQuery("#scheduled_help_popup-block").hide();
	window.setTimeout(function() {
		jQuery("#scheduled_help_popup-block").fadeIn();
		}, wait_time
	);

  // Schließen wenn Close Button gecklickt
	jQuery("#scheduled_help_popup-close a").click(function(event) {
		event.preventDefault();
		jQuery("#scheduled_help_popup-block").hide();
	});

 // Telefonnummer wird als Tooltip angezeigt.
	jQuery("#scheduled_help_popup-block-phone").aToolTip({
		clickIt: true,
		xOffset: -80
	});
});
