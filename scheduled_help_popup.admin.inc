<?php

function _scheduled_help_popup_config_settings($form, &$form_state) {

	$values = _scheduled_help_popup_get_values();

	$languages = language_list();
	ksort($languages);

	foreach ($languages as $key => $language) {
		$fieldset = 'scheduled_help_popup_' . $key;
		$form['scheduled_help_popup_' . $key] = array(
			'#type' => 'fieldset',
			'#title' => t('Language @name (@native)', array('@name' => $language->name, '@native' => $language->native)),
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
		);

		$form[$fieldset]['scheduled_help_popup_text_' . $key] = array(
			'#type' => 'textarea',
			'#title' => t('Message'),
			'#description' => t('Set the HTML text that is to be displayed in the message box (must consist of a "&lt;h2&gt;" and a "&lt;p&gt;" element).'),
			'#required' => TRUE,
			'#cols' => 80,
			'#default_value' => $values[$key]['text'],
		);

		$form[$fieldset]['scheduled_help_popup_mail_' . $key] = array(
			'#type' => 'textfield',
			'#title' => t('Mail'),
			'#description' => t('Enter the mail address of the person to be contacted.'),
			'#required' => TRUE,
			'#default_value' => $values[$key]['mail'],
		);

		$form[$fieldset]['scheduled_help_popup_phone_' . $key] = array(
			'#type' => 'textfield',
			'#title' => t('Phone number'),
			'#description' => t('Enter the phone number of the person to be contacted.'),
			'#required' => TRUE,
			'#default_value' => $values[$key]['phone'],
		);
	}

	$form['scheduled_help_popup_wait_time'] = array(
		'#type' => 'textfield',
		'#title' => t('Time to wait for popup'),
		'#description' => t('Enter the time in seconds when the box will popup.'),
		'#required' => TRUE,
		'#default_value' => $values['wait_time'],
		'#element_validate' => array('element_validate_integer_positive'),
	);

	$form['#submit'][] = '_scheduled_help_popup_config_settings_submit';
	
	$form['#validate'] = array('_scheduled_help_popup_config_settings_validate');

	return system_settings_form($form);
}

function _scheduled_help_popup_config_settings_submit($form, &$form_state) {

	$values = array(
		'wait_time' => $form_state['values']['scheduled_help_popup_wait_time'],
	);
	$languages = array_keys(language_list());
	foreach ($languages as $language) {
		$values[$language]['text'] = $form_state['values']['scheduled_help_popup_text_' . $language];
		$values[$language]['mail'] = $form_state['values']['scheduled_help_popup_mail_' . $language];
		$values[$language]['phone'] = $form_state['values']['scheduled_help_popup_phone_' . $language];
	}

	variable_set('scheduled_help_popup', $values);
}

function _scheduled_help_popup_config_settings_validate($form, &$form_state) {

	$languages = array_keys(language_list());
	foreach ($languages as $language) {
		if (!valid_email_address($form_state['values']['scheduled_help_popup_mail_' . $language])) {
			form_set_error('scheduled_help_popup_mail_' . $language, 
				t('Please enter a valid email address.'));
		}
		$text = $form_state['values']['scheduled_help_popup_text_' . $language];
		if (!strstr($text, '<h2>') || !strstr($text, '<p>')) {
			form_set_error('scheduled_help_popup_text_' . $language, 
				t('Please enter a valid HTML text containing a "&lt;h2&gt;" and a "&lt;p&gt;" element.'));
		}
	}
}
